package io.ipfs.chat.pubsub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import com.google.gson.Gson;

import io.ipfs.api.IPFS;
import io.ipfs.multihash.Multihash;

/**
 * Main class/handler for P2P communications
 * @author vaultec
 */
public class PubSubCommunicator extends Thread {
	
	
	private final PubSubCallback callback;
	private final IPFS ipfs;
	private final String channel = "ipfs-chat";
	
	public ArrayList<Multihash> peers = new ArrayList<Multihash>();
	private Thread pingThread;
	
	public PubSubCommunicator(IPFS ipfs, PubSubCallback callback) {
		this.ipfs = ipfs;
		this.callback = callback;
		
		pingThread = new Thread() {
			public void run() {
				for( ; ;) {
					for(Multihash e : peers) {
						System.out.println("Pinging " + e.toBase58());
						ping(e);
					}
					try {
						TimeUnit.SECONDS.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
	}
	
	private void ping(Multihash peer) {
		try {
			Map obj = ipfs.ping(peer);
			System.out.println(new Gson().toJson(obj));
		} catch (IOException e) {
			e.printStackTrace(); //Bad error occurred.
		}
	}
	public boolean send(String message) {
		try {
			this.ipfs.pubsub.pub(this.channel, message);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public String IPFSID() {
		try {
			return ipfs.id().get("ID").toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void run() {
		//Keep alive thread for peers;
		pingThread.start();
		Stream<Map<String, Object>> stream = null;
		try {
			stream = ipfs.pubsub.sub(this.channel);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Iterator<Map<String, Object>> it = stream.iterator();
		for( ; ; ) {
			
			Map<String, Object> packet = it.next();
			System.out.println(new Gson().toJson(packet));
			callback.debugPacketIn(packet);
			
		}
	}
}
