package io.ipfs.chat.pubsub;

import java.util.Map;

public interface PubSubCallback {
	public void debugPacketIn(Map<String, Object> packet);
}
