package io.ipfs.chat;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.chat.Datagram.ChatMessage;
import io.ipfs.chat.Datagram.gramtype;
import io.ipfs.chat.pubsub.PubSubCallback;
import io.ipfs.chat.pubsub.PubSubCommunicator;
import io.ipfs.cid.Cid;
import io.ipfs.multihash.Multihash;

public class ApplicationWindow implements PubSubCallback {

	private JFrame frmIpfsChat;
	private final Action action = new ActionSendButton();
	private DebugMenu debugMenu;
	private UploadsWindow uploadWindow;
	public PubSubCommunicator communicator;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApplicationWindow window = new ApplicationWindow();
					window.frmIpfsChat.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private IPFS ipfs;
	/**
	 * Create the application.
	 */
	public ApplicationWindow() {
		this.ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
		this.communicator = new PubSubCommunicator(this.ipfs, this);
		this.communicator.start();
		initialize();
	}
	
	private JTextField textField_1;
	private TextArea chatlogArea;
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIpfsChat = new JFrame();
		frmIpfsChat.setTitle("IPFS Chat");
		frmIpfsChat.setBounds(100, 100, 629, 525);
		frmIpfsChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIpfsChat.getContentPane().setLayout(null);
		
		chatlogArea = new TextArea();
		chatlogArea.setEditable(false);
		chatlogArea.setText("Nothing here.. Looks like you are alone.\r\n");
		chatlogArea.setBounds(0, 45, 565, 245);
		frmIpfsChat.getContentPane().add(chatlogArea);
		
		JLabel lblNewLabel = new JLabel("Chat/log box");
		lblNewLabel.setBounds(74, 23, 88, 16);
		frmIpfsChat.getContentPane().add(lblNewLabel);
		
		JButton sendButton = new JButton("Send");
		//btnNewButton.addActionListener(new SwingAction());
		sendButton.setAction(this.action);
		sendButton.setBackground(Color.GRAY);
		sendButton.setBounds(0, 354, 162, 26);
		frmIpfsChat.getContentPane().add(sendButton);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Arial", Font.PLAIN, 12));
		textField_1.setBounds(0, 296, 359, 45);
		frmIpfsChat.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		textField_1.setAction(action);
		
		JButton btnDebug = new JButton("Debug");
		btnDebug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(debugMenu == null) {
					debugMenu = new DebugMenu(self());
					debugMenu.setVisible(true);
				} else {
					if(!debugMenu.isActive()) {
						debugMenu.setVisible(true);
					}
				}
			}
		});
		
		btnDebug.setBackground(Color.WHITE);
		btnDebug.setBounds(470, 296, 98, 26);
		frmIpfsChat.getContentPane().add(btnDebug);
		
		JButton btnNewButton = new JButton("Upload file");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & GIF Images", "jpg", "gif");
				chooser.setFileFilter(filter);
				int returnVal = chooser.showOpenDialog(self().frmIpfsChat);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					System.out.println("You chose to open this file: " + chooser.getSelectedFile());
					UploadFileOrFolder(chooser.getSelectedFile());
				}
			}
		});
		btnNewButton.setBackground(Color.GRAY);
		btnNewButton.setBounds(185, 354, 98, 26);
		frmIpfsChat.getContentPane().add(btnNewButton);
		
		JButton OpenUploadsButton = new JButton("Uploads");
		OpenUploadsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(uploadWindow == null) {
					uploadWindow = new UploadsWindow(self());
					uploadWindow.setVisible(true);
				} else {
					if(!uploadWindow.isActive()) {
						uploadWindow.setVisible(true);
					}
				}
			}
		});
		OpenUploadsButton.setBackground(Color.WHITE);
		OpenUploadsButton.setBounds(360, 296, 98, 26);
		frmIpfsChat.getContentPane().add(OpenUploadsButton);
		
		/*JTextPane txtpnFileNotFound = new JTextPane();
		txtpnFileNotFound.setEditable(false);
		txtpnFileNotFound.setContentType("text/html");
		txtpnFileNotFound.setText("<h1>File not found please contact:<a href='mailto:michael@uml.com'>e-mail to</a> or call 963</h1>");

		txtpnFileNotFound.setBounds(233, 378, 234, 76);
		JScrollPane jsp = new JScrollPane(txtpnFileNotFound);
		frmIpfsChat.getContentPane().add(txtpnFileNotFound);*/
		startup();
	}
	private void startup() {
		chatlogArea.append("My IPFS ID is " + this.communicator.IPFSID() + " \r\n");
	}
	
	private void UploadFileOrFolder(File in) {
		NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(in);
		List<MerkleNode> MerkleList = null;
		try {
			MerkleList = this.ipfs.add(file);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		MerkleNode node = MerkleList.get(0);
	}
	
	private ApplicationWindow self() {
		return this;
	}
	
	private class ActionSendButton extends AbstractAction {
		public ActionSendButton() {
			putValue(NAME, "Send");
			putValue(SHORT_DESCRIPTION, "Write your message here!");
		}
		public void actionPerformed(ActionEvent e) {
			String msg = textField_1.getText();
			if(msg.equals("")) {
				return; //Check to prevent spam
			}
			ChatMessage chatmsg = new Datagram.ChatMessage();
			chatmsg.message = msg;
			communicator.send(chatmsg.encode());
			//communicator.send(Base64.getEncoder().encodeToString(msg.getBytes()));
			textField_1.setText("");
		}
	}
	
	@Override
	public void debugPacketIn(Map<String, Object> packet) {
		String raw_message = packet.get("data").toString();
		
		String JSONMessage = new String(Base64.getDecoder().decode(Base64.getDecoder().decode(raw_message.getBytes()))); //Double decode from base64.
		Multihash from = Cid.cast(Base64.getDecoder().decode(packet.get("from").toString().getBytes()));
		Map<String, Object> json = new Gson().fromJson(JSONMessage, new TypeToken<Map<String, Object>>(){}.getType());
		gramtype type = Datagram.gramtype.valueOf(json.get("type").toString());
		//Datagram BaseMessage = new Gson().fromJson(JSONMessage, Datagram.class);
		
		
		if(type == gramtype.chatmsg) {
			//ChatMessage msg = (ChatMessage) BaseMessage;
			ChatMessage msg = new Gson().fromJson(JSONMessage, ChatMessage.class);
			this.chatlogArea.append(from.toBase58() + ": " + msg.message + "\n");
		} else {
			
		}
		
	}
}
