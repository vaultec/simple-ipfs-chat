package io.ipfs.chat;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.border.LineBorder;

import com.google.gson.Gson;

import io.ipfs.multihash.Multihash;

import java.awt.Color;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JButton;

public class DebugMenu extends JFrame {

	private JPanel contentPane;
	private JList<Object> list;
	private JTextField textPeerConnect;
	public DefaultListModel<Object> peerListModel;
	ApplicationWindow appWindow;
	
	/**
	 * Create the frame.
	 */
	public DebugMenu(ApplicationWindow applicationWindow) {
		this.appWindow = applicationWindow;
		setBounds(100, 100, 597, 509);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() == 1) {
					//System.out.println(list.getSelectedIndex());
				}
			}
		});
		
		list.addMouseListener(new PopClickListener());
		list.setValueIsAdjusting(true);
		peerListModel = new DefaultListModel();
		/*	ArrayList<Object> values = new ArrayList<Object>();
			public int getSize() {
				return values.size();
			}
			public Object getElementAt(int index) {
				return values.get(index);
			}
			public void addElement(Object in) {
				System.out.println(in.toString());
				values.add(in);
			}

		};*/
		list.setModel(peerListModel);
		
		//list.getModel().getElementAt(0);
		list.setBorder(new LineBorder(new Color(0, 0, 0)));
		list.setBounds(320, 95, 227, 166);
		contentPane.add(list);
		
		textPeerConnect = new JTextField();
		textPeerConnect.setBounds(10, 353, 195, 20);
		contentPane.add(textPeerConnect);
		textPeerConnect.setColumns(10);
		
		JButton btnNewButton = new JButton("Connect");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String peerStr = textPeerConnect.getText();
				Multihash peer = Multihash.fromBase58(peerStr);
				if(!peerListModel.contains(peer.toBase58()) && !peer.toBase58().equals(appWindow.communicator.IPFSID())) {
					peerListModel.addElement(peer.toBase58());
					appWindow.communicator.peers.add(peer);
				}
			}
		});
		btnNewButton.setBounds(204, 352, 89, 23);
		contentPane.add(btnNewButton);
	}
	public class PopClickListener extends MouseAdapter {
		class PopUpDemo extends JPopupMenu {
		    JMenuItem anItem;
		    public PopUpDemo() {
		        anItem = new JMenuItem("Disconnect");
		        
		        anItem.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						list.getSelectedIndex();
						String str = list.getSelectedValue().toString();
						
						
						
						System.out.println(new Gson().toJson(e));
					}
		        });
		        add(anItem);
		    }
		}
	    public void mousePressed(MouseEvent e) {
	        if (e.isPopupTrigger())
	            doPop(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        if (e.isPopupTrigger())
	            doPop(e);
	    }

	    private void doPop(MouseEvent e){
	        PopUpDemo menu = new PopUpDemo();
	        menu.show(e.getComponent(), e.getX(), e.getY());
	    }
	}
}
