package io.ipfs.chat;

import java.util.Base64;

import com.google.gson.Gson;

import io.ipfs.multihash.Multihash;

@SuppressWarnings("unused")
public interface Datagram {
	public gramtype type();
	public String encode(); 
	
	public enum gramtype {
		base, chatmsg, mediamsg;
	}
	
	/**
	 * Example base message.
	 * @author vaultec
	 */
	public class BaseMessage implements Datagram {
		protected gramtype type = gramtype.base;
		
		@Override
		public gramtype type() {
			return type;
		}
		/**
		 * Encode to base64 json
		 * @return
		 */
		public String encode() {
			byte[] serializedBytes = new Gson().toJson(this).getBytes();
			return Base64.getEncoder().encodeToString(serializedBytes);
		}
		

	}
	
	public class ChatMessage implements Datagram {
		private final gramtype type = gramtype.chatmsg;
		
		public String message;
		public gramtype type() {
			return type;
		}
		public String encode() {
			byte[] serializedBytes = new Gson().toJson(this).getBytes();
			return Base64.getEncoder().encodeToString(serializedBytes);
		}
	}
	public class MediaMessage implements Datagram {
		private final gramtype type = gramtype.mediamsg;
		
		public String note;
		public Multihash file;
		public Multihash from;
		
		public gramtype type() {
			return type;
		}
		public String encode() {
			byte[] serializedBytes = new Gson().toJson(this).getBytes();
			return Base64.getEncoder().encodeToString(serializedBytes);
		}
	}
}
