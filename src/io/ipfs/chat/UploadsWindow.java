package io.ipfs.chat;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import io.ipfs.multihash.Multihash;

public class UploadsWindow extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private final ApplicationWindow appWindow;
	
	/**
	 * Create the frame.
	 */
	public UploadsWindow(ApplicationWindow applicationWindow) {
		this.appWindow = applicationWindow;
		setResizable(false);
		setBounds(100, 100, 582, 432);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{Multihash.fromBase58("QmRyg4HjMEfJKYbAh1gqSSMDVvVs6VFozaPUSi18aSzKVP"), null, new Timestamp(System.currentTimeMillis())},
				
			},
			new String[] {
				"File Multihash", "From", "Time", "File name"
			}
		) {
			private static final long serialVersionUID = -5847110447917580981L;
			Class[] columnTypes = new Class[] {
					Multihash.class, Multihash.class, Timestamp.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(2).setCellRenderer(new TimestampCellRenderer());
		table.setAutoCreateRowSorter(true);
		table.setBounds(10, 23, 340, 203);
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		scrollPane.setBounds(10, 11, 556, 381);
		contentPane.add(scrollPane);
	}
	class TimestampCellRenderer extends DefaultTableCellRenderer {

	    DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss a z");

	    public TimestampCellRenderer() {
	        super();
	    }

	    public void setValue(Object value) {
	        if (formatter == null) {
	            formatter = DateFormat.getDateInstance();
	        }
	        formatter.setTimeZone(TimeZone.getDefault());
	        setText((value == null) ? "" : formatter.format(value));
	    }
	}
}
